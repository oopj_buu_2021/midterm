package com.boonanan.midterm;

public class Map {
    private int width;
    private int height;
    private Unit units[];
    private int unitCount;

    public Map(int width, int height) {
        this.width = width;
        this.height = height;
        this.units = new Unit[width * height];
        this.unitCount = 0;
    }
    public Map() {
        this(10, 10);
    }

    public void print() {
        for (int i = 0; i < this.height; i++) {
            for (int o = 0; o < this.width; o++) {
                printBlock(o,i);
            }
            System.out.println();
        }
    }
    public void printBlock(int x, int y) {
        for (int i = 0; i < unitCount; i++) {
            Unit unit = this.units[i];
            if(unit.isON(x,y)){
                System.out.print(unit.getSymbol());
                return;
            }
        }
        System.out.print('-');
    }

    public String toString() {
        return "Map(" + this.width + ',' + this.height + ')';
    }
    public void add(Unit unit) {
        if (unitCount == (width * height))
            return;
        this.units[unitCount] = unit;
        unitCount++;
    }
    public void printUnit() {
        for (int i = 0; i < unitCount; i++) {
            System.out.println(this.units[i]);
        }
    }

    public boolean isOn(int x, int y) {
        return isInHeight(y) && isInWidth(x);
    }
    public boolean isInWidth(int x) {
        return x>=0 && x < width;
    }
    public boolean isInHeight(int y) {
        return y>=0 && y < height;
    }

    public boolean hasDominate(int x, int y) {
        for (int i = 0; i < unitCount; i++) {
            Unit unit = this.units[i];
            if(unit.isDominate() && unit.isON(x, y)) {
                return gameOver();
            }
        }
        return false;
    }
    public boolean gameOver() {
        System.out.println("Game Over!!");
        System.exit(0);
        return true;
    }

}
