package com.boonanan.midterm;

import java.util.Scanner;

public class App {
    static Scanner sc = new Scanner(System.in);
    static Map map = new Map(20, 20);
    static People Male = new People(map, 'M', 0, 19);
    static People FeMale1 = new People(map, 'P', 19, 0);
    static People FeMale2 = new People(map, 'G', 8, 8);
    static Building building1 = new Building(map, 5, 7);
    static Building building2 = new Building(map, 7, 8);
    static Building building3 = new Building(map, 4, 10);
    static Building building4 = new Building(map, 7, 7);
    static Building building5 = new Building(map, 6, 2);
    static Building building6 = new Building(map, 7, 12);
    static Building building7 = new Building(map, 9, 15);
    static Building building8 = new Building(map, 12, 15);
    static Building building9 = new Building(map, 18, 6);
    static Building building10 = new Building(map, 16, 8);
    static Building building11 = new Building(map, 13, 14);
    static Building building12 = new Building(map, 13, 17);

    public static String input() {
        return sc.next();
    }

    public static void Process(String command) {
        switch (command) {
            case "info":
                System.out.println("เกมไล่จับระหว่าง ฝ่ายชาย ฝ่ายหญิงและฝ่ายอิจฉา โดย ฝ่ายชายและหญิงต้องเข้าหากันให้ได้ โดยที่ฝ่ายอิจฉาจะคอยขัดขวาง และมีตึกขวางเช่นกัน (H)");
                System.out.println("มีให้ทั้ง 20 ตา ฝ่ายชายเดินได้ 2 ก้าว ฝ่ายหญิงเดินได้ 1 ก้าว ฝ่ายอิจฉาเดินได้ 3 ก้าว");
                System.out.println("พิมพ์ชื่อตัวละครตามด้วยทิศทาง(ms,pw,gd) หากต้องการออกให้พิมพ์ q");
                break;
            case "mw":
                Male.up(2);
                break;
            case "ma":
                Male.left(2);
                break;
            case "ms":
                Male.down(2);
                break;
            case "md":
                Male.right(2);
                break;
            case "pw":
                FeMale1.up(1);
                break;
            case "pa":
                FeMale1.left(1);
                break;
            case "ps":
                FeMale1.down(1);
                break;
            case "pd":
                FeMale1.right(1);
                break;
            case "gw":
                FeMale2.up(3);
                break;
            case "ga":
                FeMale2.left(3);
                break;
            case "gs":
                FeMale2.down(3);
                break;
            case "gd":
                FeMale2.right(3);
                break;
            case "q":
                System.exit(0);
                break;
            default:
                break;
        }
    }

    public static void main(String[] args) {
        int n = 1;
        map.add(Male);
        map.add(FeMale1);
        map.add(FeMale2);
        map.add(building1);
        map.add(building2);
        map.add(building3);
        map.add(building4);
        map.add(building5);
        map.add(building6);
        map.add(building7);
        map.add(building8);
        map.add(building9);
        map.add(building10);
        map.add(building11);
        map.add(building12);
        

        while (n < 21) {
            map.print();
            System.out.println("Round " + n);
            String command = input();
            Process(command);
            n++;
        }
        System.exit(0);
    }
}
