package com.boonanan.midterm;

public class People extends Unit {

    public People(Map map, char symbol, int x, int y) {
        super(map, symbol, x, y, true);
    
    }

    public void up(int step) {
        int y = this.getY();
        y-=step;
        this.setY(y);
    }
    public void down(int step) {
        int y = this.getY();
        y+=step;
        this.setY(y);
    }
    public void left(int step) {
        int x = this.getX();
        x-=step;
        this.setX(x);
    }
    public void right(int step) {
        int x = this.getX();
        x+=step;
        this.setX(x);
    }

    
    
}
